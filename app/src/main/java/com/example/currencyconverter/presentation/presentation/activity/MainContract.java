package com.example.currencyconverter.presentation.presentation.activity;

import com.example.currencyconverter.presentation.presentation.base.BasePresenter;

import java.util.List;

public interface MainContract {
    interface View {
        void initListAdapter(List<String> list);

        void showResult(double val);

        void addItem(String item);
    }

    interface Presenter extends BasePresenter<View> {
        void init();

        void eventOperation(String value, String val1, String val2);
    }
}
