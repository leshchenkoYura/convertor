package com.example.currencyconverter.presentation.data.exception;

import androidx.annotation.Nullable;

public class AppException extends Exception {
    private String message;
    public static final String ZERO_SIZE = "zeroSize";

    public AppException(String message) {
        super(message);
        this.message = message;
    }


    @Nullable
    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
