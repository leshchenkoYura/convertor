package com.example.currencyconverter.presentation.app;

import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.example.currencyconverter.presentation.Constant;
import com.example.currencyconverter.presentation.data.Converter;
import com.example.currencyconverter.presentation.data.local.ConverterDataBase;
import com.example.currencyconverter.presentation.data.local.LocalServiceEntry;

import timber.log.Timber;

public class App extends Application {
public static  LocalServiceEntry local;
    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        //Converter.getInstance();
        local = provideLocaleDao(provideRoomDataBase());
    }

    private ConverterDataBase provideRoomDataBase() {
        return Room.databaseBuilder(
                this, ConverterDataBase.class, Constant.NAME_DAO)
                .build();
    }

    private LocalServiceEntry provideLocaleDao(ConverterDataBase dao) {
        return dao.getConverterDao();
    }


}
