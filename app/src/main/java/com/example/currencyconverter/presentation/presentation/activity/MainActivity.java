package com.example.currencyconverter.presentation.presentation.activity;


import android.annotation.SuppressLint;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.currencyconverter.R;
import com.example.currencyconverter.presentation.presentation.adapter.AdapterMain;
import com.example.currencyconverter.presentation.presentation.base.BaseActivity;
import com.example.currencyconverter.presentation.presentation.base.BasePresenter;

import java.util.List;

public class MainActivity extends BaseActivity implements MainContract.View {
    MainContract.Presenter presenter;
    EditText sum;
    EditText res;
    Spinner spin1;
    Spinner spin2;
    private AdapterMain adapter;
    private AppCompatButton button;


    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        presenter = new MainPresenter();
        sum = findViewById(R.id.valueForConvert);
        res = findViewById(R.id.convertResult);
        spin1 = findViewById(R.id.spinner);
        spin2 = findViewById(R.id.spinner2);
        button = findViewById(R.id.calc);
        button.setOnClickListener(v ->{
            String val1 = spin1.getSelectedItem().toString();
            String val2 = spin2.getSelectedItem().toString();
            String value= sum.getText().toString();
            presenter.eventOperation(value,val1,val2);
        });
        RecyclerView recyclerView = findViewById(R.id.rvView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new AdapterMain();
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
        presenter.init();
    }

    @Override
    protected void onDestroyView() {
        if (presenter != null) presenter = null;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void initListAdapter(List<String> list) {
        adapter.initListItem(list);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void showResult(double val) {
        res.setText(String.format("%f",val));

    }

    @Override
    public void addItem(String item) {
        adapter.addItem(item);
    }


}
