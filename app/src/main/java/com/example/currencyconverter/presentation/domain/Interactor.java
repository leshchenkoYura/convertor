package com.example.currencyconverter.presentation.domain;

import com.example.currencyconverter.presentation.data.Converter;
import com.example.currencyconverter.presentation.data.Repository;
import com.example.currencyconverter.presentation.data.RepositoryContract;
import com.example.currencyconverter.presentation.data.exception.AppException;
import com.example.currencyconverter.presentation.data.model.dao.Entry;
import com.example.currencyconverter.presentation.domain.base.BaseInteractor;
import java.util.ArrayList;
import java.util.List;
import io.reactivex.Single;


public class Interactor extends BaseInteractor implements InteractorContract {
    private RepositoryContract repository;

    public Interactor() {
        repository = new Repository();
    }

    @Override
    public Single<String> insertDao(String value, String val1, String val2) {
        return Single.just(new Entry(conver(val1,val2,value)))
                .flatMap(v -> Single.just(repository.insert(v)))
                .flatMap(v -> repository.queryEntry(v))
                .map(Entry::getResult)
                .compose(applySingleSchedulers());


    }

    @Override
    public Single<List<String>> getListDao() {
        return repository.qeuryList()
                .flatMap(v ->{
                    if (v != null && v.size() > 0){
                        return Single.just(v);
                    }else {
                       return Single.error(new AppException(AppException.ZERO_SIZE));
                    }
                })
                .flatMap(v ->{
                    List<String> list = new ArrayList<>();
                    for (int i = 0; i < v.size(); i++) {
                        list.add(v.get(i).getResult());
                    }
                    return Single.just(list);
                })
                .compose(applySingleSchedulers());
    }


    private String conver(String val1,String val2,String val3){
        double val = Double.parseDouble(val3);
        return String.valueOf(Converter.getInstance().convertValue(val1,val2,val));
    }
}
