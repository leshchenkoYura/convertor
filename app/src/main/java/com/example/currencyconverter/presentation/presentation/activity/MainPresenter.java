package com.example.currencyconverter.presentation.presentation.activity;

import com.example.currencyconverter.presentation.domain.Interactor;
import com.example.currencyconverter.presentation.domain.InteractorContract;
import java.util.List;
import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;

public class MainPresenter implements MainContract.Presenter {
    private MainContract.View view;
    private InteractorContract interactor;

    public MainPresenter() {
        interactor = new Interactor();
    }
    @Override
    public void startView(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void init() {
        interactor.getListDao()
                .subscribe(new DisposableSingleObserver<List<String>>() {
                    @Override
                    public void onSuccess(List<String> list) {
                        view.initListAdapter(list);
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e("getListDao %s",e.getMessage());
                    }
                });
    }

    @Override
    public void eventOperation(String value,String val1,String val2) {
        interactor.insertDao(value,val1,val2)
               .subscribe(new DisposableSingleObserver<String>() {
                   @Override
                   public void onSuccess(String s) {
                        view.showResult(Double.parseDouble(s));
                        view.addItem(s);
                        dispose();
                   }

                   @Override
                   public void onError(Throwable e) {
                       Timber.e("insertDao %s",e.getMessage());
                   }
               });

    }



    @Override
    public void detachView() {
        if (view != null) view = null;
        if (interactor != null) interactor = null;
    }
}
