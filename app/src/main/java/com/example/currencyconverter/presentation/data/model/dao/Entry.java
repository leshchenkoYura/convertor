package com.example.currencyconverter.presentation.data.model.dao;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity(tableName = "entry")
public class Entry {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo(name = "result")
    private String result;

    @Ignore
    public Entry() {
    }

    public Entry(@NonNull String result) {
        this.result = result;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}