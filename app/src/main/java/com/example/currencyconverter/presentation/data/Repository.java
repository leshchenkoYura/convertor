package com.example.currencyconverter.presentation.data;

import com.example.currencyconverter.presentation.app.App;
import com.example.currencyconverter.presentation.data.model.dao.Entry;
import java.util.List;
import io.reactivex.Single;

public class Repository implements RepositoryContract {


    public Repository() {
    }



    @Override
    public long insert(Entry entity) {
        return App.local.insertEntry(entity);
    }



    @Override
    public Single<List<Entry>> qeuryList() {
        return App.local.queryEntryList();
    }

    @Override
    public Single<Entry> queryEntry(long id) {
        return App.local.queryEntry(id);
    }
}
