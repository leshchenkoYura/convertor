package com.example.currencyconverter.presentation.domain;

import java.util.List;
import io.reactivex.Single;

public interface InteractorContract {
    Single<String> insertDao(String value, String val1, String val2);

    Single<List<String>> getListDao();

}
