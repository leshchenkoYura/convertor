package com.example.currencyconverter.presentation.presentation.base;

public interface BasePresenter<V> {
    void startView(V view);
    void detachView();
}
