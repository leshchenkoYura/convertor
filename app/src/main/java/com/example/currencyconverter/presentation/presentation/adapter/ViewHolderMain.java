package com.example.currencyconverter.presentation.presentation.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.currencyconverter.R;


public class ViewHolderMain extends RecyclerView.ViewHolder {

    public ViewHolderMain(@NonNull View itemView) {
        super(itemView);
    }

    public void bind(String item){
        if (item != null && !item.isEmpty()){
            AppCompatTextView textView = itemView.findViewById(R.id.tvItem);
            textView.setText(item);
        }
    }
}
