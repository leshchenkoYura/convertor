package com.example.currencyconverter.presentation.presentation.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.currencyconverter.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterMain extends RecyclerView.Adapter<ViewHolderMain> {
    private List<String> list;

    public AdapterMain() {
        list = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolderMain onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolderMain(LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false));
    }

    public void initListItem(List<String> listDao){
        list = new ArrayList<>(listDao);
        notifyDataSetChanged();
    }

    public void addItem(String item){
        list.add(item);
        notifyItemInserted(list.size() - 1);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderMain holder, int position) {
        if (list.size() != 0)holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
