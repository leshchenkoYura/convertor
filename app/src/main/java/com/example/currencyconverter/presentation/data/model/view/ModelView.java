package com.example.currencyconverter.presentation.data.model.view;

import java.util.Objects;

public class ModelView {
    private String val1;
    private String val2;
    private String val3;

    public ModelView() {
    }

    public ModelView(String val1, String val2, String val3) {
        this.val1 = val1;
        this.val2 = val2;
        this.val3 = val3;
    }

    public String getVal1() {
        return val1;
    }

    public void setVal1(String val1) {
        this.val1 = val1;
    }

    public String getVal2() {
        return val2;
    }

    public void setVal2(String val2) {
        this.val2 = val2;
    }

    public String getVal3() {
        return val3;
    }

    public void setVal3(String val3) {
        this.val3 = val3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelView modelView = (ModelView) o;
        return Objects.equals(val1, modelView.val1) &&
                Objects.equals(val2, modelView.val2) &&
                Objects.equals(val3, modelView.val3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(val1, val2, val3);
    }

    @Override
    public String toString() {
        return "ModelView{" +
                "val1='" + val1 + '\'' +
                ", val2='" + val2 + '\'' +
                ", val3='" + val3 + '\'' +
                '}';
    }
}
