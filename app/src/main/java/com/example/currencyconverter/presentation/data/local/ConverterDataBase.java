package com.example.currencyconverter.presentation.data.local;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.currencyconverter.presentation.data.model.dao.Entry;

@Database(entities = {Entry.class}, version = 1)
public abstract class ConverterDataBase extends RoomDatabase {
    public abstract LocalServiceEntry getConverterDao();
}
