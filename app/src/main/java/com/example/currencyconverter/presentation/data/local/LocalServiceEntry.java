package com.example.currencyconverter.presentation.data.local;



import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.currencyconverter.presentation.data.model.dao.Entry;
import java.util.List;
import io.reactivex.Single;

@Dao
public interface LocalServiceEntry {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertEntry(Entry entry);

    @Query("select * from entry")
    Single<List<Entry>> queryEntryList();

    @Query("SELECT * FROM entry WHERE id IS :id")
    Single<Entry> queryEntry(long id);
}
