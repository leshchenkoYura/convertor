package com.example.currencyconverter.presentation.data;

import com.example.currencyconverter.presentation.data.model.dao.Entry;
import java.util.List;
import io.reactivex.Single;

public interface RepositoryContract {
    long insert(Entry entity);

    Single<List<Entry>> qeuryList();

    Single<Entry> queryEntry(long id);
}
